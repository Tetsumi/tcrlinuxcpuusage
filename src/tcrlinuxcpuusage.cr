#
#  tcrlinuxcpuusage
#
#  Contributors:
#     Tetsumi <tetsumi@vmail.me>
#
#  Copyright (C) 2017 Tetsumi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require "option_parser"

VERSION = "1.0.0"

struct CPU
  getter id, usage

  def initialize(@id : UInt64, @usage : Float64)
  end

  def colorize
    #TODO
  end

  def <(other)
    self.usage < other.usage
  end

  def <=>(other)
    self.usage <=> other.usage
  end

  def <=(other)
    self.usage <= other.usage
  end
end

enum SortingMethod
  ID
  Most
  Least
end

def fatal_error(msg)
  abort("[ERROR] " + msg)
end

enum Column : UInt8
  Id
  User
  Nice
  System
  Idle
  IOWait
  IRQ
  SoftIRQ
  Steal
  Guest
  Guest_nice
end

#
# Read and parse /proc/stat
#
def get_cpu_times
  cpu_times = Array(Tuple(UInt64, UInt64)).new

  File.each_line("/proc/stat") do |line|
      columns = line.split
      if !columns[Column::Id.value].starts_with?("cpu")
        break
      end
      id = columns[Column::Id.value].match(/\d+/)
      if id.nil?
        next
      end
      idle = columns[Column::Idle.value].to_u64
      busy =   columns[Column::User.value].to_u64
             + columns[Column::Nice.value].to_u64
             + columns[Column::System.value].to_u64
             + columns[Column::IRQ.value].to_u64
             + columns[Column::SoftIRQ.value].to_u64
             + columns[Column::Steal.value].to_u64

      cpu_times << {idle, busy}
  end

  return cpu_times
end

#
# Compute CPU usages for the given period.
#
def get_cpu_usages(delay)
  cpu_usages = Array(CPU).new
  before_times = get_cpu_times
  sleep(Time::Span.new(Time::Span::TicksPerMillisecond * delay))
  after_times = get_cpu_times
  before_times.map_with_index { |timer, index|
    idle_delta = after_times[index][0] - timer[0]
    busy_delta = after_times[index][1] - timer[1]
    CPU.new(index.to_u64, busy_delta.fdiv(idle_delta + busy_delta) * 100.0)
  }
end

# For 256 colors terminal
Colors = [ 46, # Green1
          112, # Chartreuse2
          142, # Gold3
          190, # Yellow2
          221, # LightGoldenrod2
          226, # Yellow1
          214, # Orange1
          208, # DarkOrange
          202, # OrangeRed1
          196, # Red1
          196, # Red1
         ]

def set_usage_color(usage)
  color = Colors[(usage / 10.0).to_u32]
  printf("\e[38;5;#{color}m")
end

def reset_color
  printf("\e[0m")
end

#
# Default config
#
color           = false
ids             = Array(UInt64).new # If empty, show all CPUs
arithmetic_mean = false
sorting_method  = SortingMethod::ID
delay           = 128
id_format       = ""
usage_format    = "%3.f%%"
separator       = " "

def print_default_config
  puts " --color=false"
  puts " --delay=128"
  puts " --idformat=\"\""
  puts " --list="
  puts " --usageformat=\"%3.f%%\""
  puts " --separator=\" \""
  puts " --sort=id"
end

#
# Parse the parameters line
#
OptionParser.parse! do |parser|
  parser.banner = "Usage: tcrlinuxcpuusage [arguments]"
  parser.on("-c", "--color", "Colorize the output") { color = true }
  parser.on("--defaultconfig", "Show default config") {
    print_default_config
    exit
  }
  parser.on("-d milliseconds",
            "--delay=milliseconds",
            "Set for which time period to get the CPU usages") { |ms|
              begin
                delay = ms.to_u64
              rescue e : ArgumentError
                fatal_error("Bad input for --delay: #{ms}")
              end

            }
  parser.on("-h", "--help", "Show this help") {
    puts parser
    exit
  }
  parser.on("--idformat=fmt",
            "Set printf format for printing the CPU ids") { |fmt|
    id_format = fmt
  }
  parser.on("-l ids",
            "--list=ids",
            "Specify by id which CPUs to show") { |ids_str|
              ids_str.split(",") { |s|
                begin
                  ids << s.to_u64
                rescue e : ArgumentError
                  fatal_error("Bad input for --list: #{s}")
                end
              }
  }
  parser.on("-m", "--mean", "Show an arithmetic mean of all CPU usages") {
    arithmetic_mean = true
  }
  parser.on("--usageformat=fmt",
            "Set printf format for printing the CPU usages") { |fmt|
    usage_format = fmt
  }
  parser.on("--separator=sep", "Set separator string") { |sep|
    separator = sep
  }
  parser.on("-s",
            "--sort=method",
            "How output is sorted. choices are id, most, and least") { |meth|
              begin
                sorting_method = SortingMethod.parse(meth)
              rescue e : ArgumentError
                fatal_error("Bad input for --sorting: #{meth}")
              end
            }
  parser.on("-v", "--version", "Show version") {
    puts VERSION
    exit
  }
end

cpu_usages = get_cpu_usages(delay)
case sorting_method
when SortingMethod::Least
  cpu_usages.sort!
when SortingMethod::Most
  cpu_usages.sort! { |a,b| b <=> a}
end
mean = 0.0
print_sep = false
cpu_usages.each_with_index { |cpu, i|
  if ids.size > 0 && !ids.includes?(cpu.id)
    next
  end
  if print_sep
    printf(separator)
  end
  print_sep = true
  printf(id_format, cpu.id)
  if color
    set_usage_color(cpu.usage)
  end
  printf(usage_format, cpu.usage)
  if color
    reset_color
  end
  mean += cpu.usage
}
if arithmetic_mean
  mean /= cpu_usages.size
  printf(separator)
  if color
    set_usage_color(mean)
  end
  printf(usage_format, mean)
  if color
    reset_color
  end
end
puts
