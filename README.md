tcrlinuxcpuusage
============================

A command for Linux to retrieve the current CPU usage. Written in Crystal.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

How to compile
----------------------------

Open a terminal in the project folder then use the command `shards build` to
build an executable in the `bin\` folder.

Manual
----------------------------

Use `tcrlinuxcpuusage --help`

FAQ
----------------------------

### How the CPU usage is computed?

The program first retrieves the CPU timers by reading the file `/proc/stat` from
[procfs](https://www.wikiwand.com/en/Procfs).
Here a example output from `/proc/stat`

```
$ cat /proc/stat
cpu  419507 170 24200 1546083 15654 0 1158 0 0 0
cpu0 210054 82 12068 772423 8058 0 558 0 0 0
cpu1 209452 87 12131 773659 7595 0 600 0 0 0
intr 8878112 49 3 0 0 0 0 0 0 1 0 0 0 4 0 0 0
ctxt 37497201
btime 1495872242
processes 11851
procs_running 1
procs_blocked 1
softirq 4668891 12 1542207 462808 851900 66428 0 305096 1088447 0 351993
```

The lines that interest us are

```
cpu  419507 170 24200 1546083 15654 0 1158 0 0 0
cpu0 210054 82 12068 772423 8058 0 558 0 0 0
cpu1 209452 87 12131 773659 7595 0 600 0 0 0
```

To know what these columns do mean, we have to look in the file `fs/proc/stat.c`
from the Linux source code.  In the procedure
[`show_stat()`](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/tree/fs/proc/stat.c?h=v4.11.3#n81)
, we can see

```.c
seq_put_decimal_ull(p, "cpu  ", nsec_to_clock_t(user));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(nice));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(system));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(idle));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(iowait));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(irq));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(softirq));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(steal));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(guest));
seq_put_decimal_ull(p, " ", nsec_to_clock_t(guest_nice));
seq_putc(p, '\n');
```

We now know that the fifth column is the idling time. Now we have to know how the
kernel compute the idling time to being able to compute a percentage. We will
find the answer in the procedure
[`get_cpu_idle_time_jiffy()`](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/tree/drivers/cpufreq/cpufreq.c?h=v4.11.3#n129)
from the file `drivers/cpufreq/cpufreq.c`

```.c
static inline u64 get_cpu_idle_time_jiffy(unsigned int cpu, u64 *wall)
{
    u64 idle_time;
    u64 cur_wall_time;
    u64 busy_time;

    cur_wall_time = jiffies64_to_nsecs(get_jiffies_64());

    busy_time = kcpustat_cpu(cpu).cpustat[CPUTIME_USER];
    busy_time += kcpustat_cpu(cpu).cpustat[CPUTIME_SYSTEM];
    busy_time += kcpustat_cpu(cpu).cpustat[CPUTIME_IRQ];
    busy_time += kcpustat_cpu(cpu).cpustat[CPUTIME_SOFTIRQ];
    busy_time += kcpustat_cpu(cpu).cpustat[CPUTIME_STEAL];
    busy_time += kcpustat_cpu(cpu).cpustat[CPUTIME_NICE];

    idle_time = cur_wall_time - busy_time;
    if (wall)
        *wall = div_u64(cur_wall_time, NSEC_PER_USEC);

    return div_u64(idle_time, NSEC_PER_USEC);
}
```

We now know that the busy time is `user + nice + system + irq + softirq + steal`
which are respectively the 2nd, 3rd, 4th, 7th, 8th, and 9th columns from
`\proc\stat`.

Those timers being incremented since the CPU has been powered on, we need to
repeat this procedure after waiting a period to compute a delta. Then, we can
finally compute the CPU usage for the said period with the following formula

```
USAGE = (IDLE_TIME_DELTA / (BUSY_TIME_DELTA + IDLE_TIME_DELTA)) * 100
```
